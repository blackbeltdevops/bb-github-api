ARG SRC_DIR=src/bitbucket.org/blackbeltdevops/bb-github-api

# build stage
FROM iron/go:dev AS build-env
ARG SRC_DIR
WORKDIR /go
COPY .  $SRC_DIR
RUN cd  $SRC_DIR && rm -r test ; mkdir test && \
    go get -t -d && go get github.com/stretchr/testify && \
    go test -v ./... && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main

# final stage#
FROM alpine
ARG SRC_DIR
WORKDIR /app
COPY --from=build-env /go/$SRC_DIR/main /app/
ENTRYPOINT ["/app/main"]

