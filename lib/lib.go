package lib

import (
    "fmt"
    "github.com/google/go-github/github"
    "strings"
    "encoding/json"
    "context"
)

type Dict map[string]string

func (d Dict) Json() string {
    payload, _ := json.Marshal(d)
    return string(payload)
}

func IsEmpty(i int) bool {
    return i == 0
}

func Check(e error) {
    if e != nil {
        fmt.Println("PANIIIICCC")
        panic(e)
    }
}

func init() {
    fmt.Println("Api init ...")
}

type Permission struct {
    Push, Pull, Admin string
}

type BBString string

func (s BBString) Str() string {
    return string(s)
}

type Owner struct{ BBString `json:"owner"` }
type Repo struct{ BBString `json:"repository"` }
type User struct{ BBString `json:"user"` }
type Path struct{ BBString `json:"path"` }

type Invite struct {
    Owner
    Repo
    User
}
type FileCreate struct {
    Owner
    Repo
    Path
}

type HookCreate struct {
    Owner
    Repo
}

type InvError struct {
    Resp         *github.Response
    Err          error
    Id           int64
    RepoInvEmpty bool
}

type  HookPayload struct{
    H    HookCreate   `json:"hookCreateData"`
    Hook *github.Hook `json:"hook"`
}
type InvitePayload struct{
    I Invite `json:"inviteData"`
}
type FileCreatePayload struct{
    F FileCreate `json:"fileCreatingData"`
    Opts *github.RepositoryContentFileOptions `json:"repoContentOpts"`
}

type Service struct {
    Context    context.Context
    GhClient   *github.Client
}

var BuildkitePipeline = strings.Join([]string{
    "# .buildkite/pipeline.yml",
    "steps:",
    "  - name: \":hammer: build \"",
    "    command: echo \"Hello world\"",
    "#   agents:",
    "#      target: assembler",
    "",
}, "\n")

//type BBGithub struct {
//    Permission Permission
//    Service    Service
//    Invt       Invite
//    FileCreate FileCreate
//    HookCreate HookCreate
//}
