package api

import (
    "encoding/json"
    "github.com/google/go-github/github"
    "bitbucket.org/blackbeltdevops/bb-github-api/lib"
)

func CreateHookWithJson(jsonAsStr string) lib.HookPayload {
    var hPay = lib.HookPayload{}
    json.Unmarshal([]byte(jsonAsStr), &hPay)
    return hPay
}

func CreateHook(owner, repo string, hook *github.Hook) lib.HookPayload {
    var hPay = lib.HookPayload{}
    hPay.H.Repo = lib.Repo{BBString: lib.BBString(repo)}
    hPay.H.Owner = lib.Owner{BBString: lib.BBString(owner)}
    if hPay.Hook == nil {
        hPay.Hook = new(github.Hook)
        hPay.Hook.Name = github.String("web")
        hPay.Hook.Active = github.Bool(true)
        hPay.Hook.Events = []string{"push", "pull_request"}
    } else {
        hPay.Hook = hook
    }
    return hPay
}

func DecodeFileCreateData(jsonAsStr string) lib.FileCreatePayload {
    var payload lib.FileCreatePayload
    json.Unmarshal([]byte(jsonAsStr), &payload)
    return payload
}

func GetOpts(jsonAsStr string) *github.RepositoryContentFileOptions {
    var payload lib.FileCreatePayload
    json.Unmarshal([]byte(jsonAsStr), &payload)
    return payload.Opts
}

func getOwnerRepo(owner, repo string) (lib.Owner, lib.Repo) {
    return lib.Owner{BBString: lib.BBString(owner)},
        lib.Repo{BBString: lib.BBString(repo)}
}

func Invite(owner, repo, user string) (lib.Owner, lib.Repo, lib.User) {
    o, r := getOwnerRepo(owner, repo)
    return o, r, lib.User{BBString: lib.BBString(user)}
}

func FileCreate(owner, repo, path string) (lib.Owner, lib.Repo, lib.Path) {
    o, r := getOwnerRepo(owner, repo)
    return o, r, lib.Path{BBString: lib.BBString(path)}
}

func InvErrorEvaluation(invError []lib.InvError) (*github.Response, error) {
    for _, ue := range invError {
        if ue.Err != nil {
            return ue.Resp, ue.Err
        }
    }
    return invError[0].Resp, invError[0].Err
}
