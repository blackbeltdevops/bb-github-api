package api

import (
    "reflect"
    "testing"

    "bitbucket.org/blackbeltdevops/bb-github-api/lib"
    "fmt"
    "encoding/json"
    "github.com/stretchr/testify/assert"
)

func TestCreateHookWithJson(t *testing.T) {

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}

    jsonAsStr := fmt.Sprintf(` { 
            "hookCreateData": {
                "owner":"%v", "repository": "%v"
            },
            "hook" : {
                "name": "web",
                "active": true,
                "events": [
                  "push",
                  "pull_request"
                ],
                "config": {
                  "url": "http://example.com/webhook",
                  "content_type": "json"
                }
            }
        }
    `, owner.Str(), repo.Str(),
    )

    wantHooks := lib.HookPayload{}
    json.Unmarshal([]byte(jsonAsStr), &wantHooks)

    assert.Equal(t, owner, wantHooks.H.Owner, "Wrong owner in lib.HookPayload")
    assert.Equal(t, repo, wantHooks.H.Repo, "Wrong repo in lib.HookPayload")
    assert.NotEmpty(t, wantHooks.Hook, "Hook is empty in lib.HookPayload")

    type args struct {
        jsonAsStr string
    }
    tests := []struct {
        name string
        args args
        want lib.HookPayload
    }{
        {"success", args{jsonAsStr}, wantHooks},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if got := CreateHookWithJson(tt.args.jsonAsStr); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("CreateHookWithJson() = %v, want %v", got, tt.want)
            }
        })
    }
}
