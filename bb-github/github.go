package bb_github

import (
    "fmt"
    "github.com/google/go-github/github"
    "golang.org/x/net/context"
    "golang.org/x/oauth2"
    "net/http"
    "strconv"
    "encoding/json"
    "bitbucket.org/blackbeltdevops/bb-github-api/lib"
)

type BBGithub struct {
    permission lib.Permission
    service    lib.Service
    invt       lib.Invite
    fileCreate lib.FileCreate
}

func MakeHttpClient(token string) *http.Client {
    return oauth2.NewClient(
        context.Background(),
        oauth2.StaticTokenSource(&oauth2.Token{AccessToken: token}),
    )
}
func GetGithubClient(cli *http.Client) *github.Client {
    return github.NewClient(cli)
}
func NewClientWithGithubClient(GhCli *github.Client) BBGithub {
    s := lib.Service{context.Background(), GhCli}
    return NewClient(s)
}
func NewClient(s lib.Service) BBGithub {
    p := lib.Permission{Push: "push", Pull: "pull", Admin: "admin"}
    BBGh := BBGithub{
        p,
        s,
        lib.Invite{},
        lib.FileCreate{},
    }
    return BBGh
}

func setFCPToEmptyAtError(err error, payload *lib.FileCreatePayload) {
    if err != nil {
        payload.F = lib.FileCreate{}
        payload.Opts = &github.RepositoryContentFileOptions{}
    }
}
func setOptsToDefaultWhenEmpty(p *lib.FileCreatePayload) {
    if p.Opts == nil {
        p.Opts = createDefaultRepoCntFileOpts()
    }
}
func createDefaultRepoCntFileOpts() *github.RepositoryContentFileOptions {
    return &github.RepositoryContentFileOptions{
        Message:   github.String("Add default pipeline yaml file"),
        Content:   []byte( lib.BuildkitePipeline),
        Branch:    github.String("master"),
        Committer: &github.CommitAuthor{Name: github.String("BuildFarm"), Email: github.String("bb-buildfarm@blackbelt.hu")},
    }
}

func (BBGh BBGithub) sendInvite(opt *github.RepositoryAddCollaboratorOptions) (*github.Response, error) {
    repos, err := BBGh.service.GhClient.Repositories.AddCollaborator(BBGh.service.Context,
        BBGh.invt.Owner.Str(), BBGh.invt.Repo.Str(), BBGh.invt.User.Str(), opt,
    )
    return repos, err
}
func (BBGh BBGithub) sendCreateFileRequest(opts *github.RepositoryContentFileOptions) (*github.RepositoryContentResponse, *github.Response, error) {
    repoContent, respons, err := BBGh.service.GhClient.Repositories.CreateFile(
        BBGh.service.Context, BBGh.fileCreate.Owner.Str(), BBGh.fileCreate.Repo.Str(), BBGh.fileCreate.Path.Str(), opts,
    )

    return repoContent, respons, err
}

func (BBGh BBGithub) listWebHook(hookPayload lib.HookPayload) (hooks []*github.Hook, resp *github.Response, err error) {
    hooks, resp, err = BBGh.service.GhClient.Repositories.ListHooks(
        BBGh.service.Context, hookPayload.H.Owner.Str(), hookPayload.H.Repo.Str(), nil,
    )
    return
}

func (BBGh BBGithub) CreateWebHook(hookPayload lib.HookPayload) (hook *github.Hook, resp *github.Response, err error) {
    hook, resp, err = BBGh.service.GhClient.Repositories.CreateHook(
        BBGh.service.Context, hookPayload.H.Owner.Str(), hookPayload.H.Repo.Str(), hookPayload.Hook,
    )
    return
}

func (BBGh BBGithub) Push() (*github.Response, error) {
    return BBGh.sendInvite(&github.RepositoryAddCollaboratorOptions{Permission: BBGh.permission.Push})
}
func (BBGh BBGithub) Pull() (*github.Response, error) {
    return BBGh.sendInvite(&github.RepositoryAddCollaboratorOptions{Permission: BBGh.permission.Pull})
}
func (BBGh BBGithub) Admin() (*github.Response, error) {
    return BBGh.sendInvite(&github.RepositoryAddCollaboratorOptions{Permission: BBGh.permission.Admin})
}
func (BBGh BBGithub) Invite(owner lib.Owner, repo lib.Repo, user lib.User) BBGithub {
    BBGh.invt = lib.Invite{owner, repo, user}
    return BBGh
}
func (BBGh BBGithub) InviteWithJson(jsonAsStr string) BBGithub {
    var invitePayload = lib.InvitePayload{}
    json.Unmarshal([]byte(jsonAsStr), &invitePayload)
    BBGh.invt = invitePayload.I
    return BBGh
}
func (BBGh BBGithub) Invitations() ([]*github.RepositoryInvitation, *github.Response, error) {
    return BBGh.service.GhClient.Users.ListInvitations(BBGh.service.Context, nil)
}

func (BBGh BBGithub) AddDefaultBuildkitePipeline(o lib.Owner, r lib.Repo, p lib.Path, opts *github.RepositoryContentFileOptions) (*github.RepositoryContentResponse, *github.Response, error) {
    BBGh.fileCreate = lib.FileCreate{o, r, p}
    if opts == nil {
        opts = createDefaultRepoCntFileOpts()
    }
    return BBGh.sendCreateFileRequest(opts)
}
func (BBGh BBGithub) AddPipelineWithJson(jsonAsStr string) (*github.RepositoryContentResponse, *github.Response, error) {
    var payload lib.FileCreatePayload

    setFCPToEmptyAtError(json.Unmarshal([]byte(jsonAsStr), &payload), &payload)
    setOptsToDefaultWhenEmpty(&payload)

    BBGh.fileCreate = payload.F
    return BBGh.sendCreateFileRequest(payload.Opts)
}

func (BBGh BBGithub) AcceptAllInvitations(repoInvs []*github.RepositoryInvitation, invResp *github.Response, invErr error) [] lib.InvError {
    f := func() [] lib.InvError {
        var inviteErrors [] lib.InvError
        for _, invs := range repoInvs {
            resp, err := BBGh.service.GhClient.Users.AcceptInvitation(BBGh.service.Context, *invs.ID)
            inviteErrors = append(inviteErrors, lib.InvError{Resp: resp, Err: err, Id: *invs.ID})
        }
        for _, invErr := range inviteErrors {
            if invErr.Err != nil {
                fmt.Println("Some invitation can't be done ... id: " + strconv.FormatInt(invErr.Id, 10))
            }
        }
        return inviteErrors
    }
    if ! lib.IsEmpty(len(repoInvs)) {
        return f()
    } else {
        return [] lib.InvError{{invResp, invErr, -1, true}}
    }
}
