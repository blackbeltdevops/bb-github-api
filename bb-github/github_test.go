package bb_github

import (
    "context"
    "encoding/json"
    "errors"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/http/httptest"
    "net/url"
    "os"
    "reflect"
    "strings"
    "testing"

    "bitbucket.org/blackbeltdevops/bb-github-api/lib"
    "github.com/google/go-github/github"
    "github.com/stretchr/testify/assert"
    "golang.org/x/oauth2"
)

const (
    // baseURLPath is a non-empty Client.BaseURL path to use during tests,
    // to ensure relative URLs are used for all endpoints. See issue #752.
    baseURLPath = "/api-v3"
)

func setup() (client *github.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    // mux is the HTTP request multiplexer used with the test server.
    mux = http.NewServeMux()

    // We want to ensure that tests catch mistakes where the endpoint URL is
    // specified as absolute rather than relative. It only makes a difference
    // when there's a non-empty base URL path. So, use that. See issue #752.
    apiHandler := http.NewServeMux()
    apiHandler.Handle(baseURLPath+"/", http.StripPrefix(baseURLPath, mux))
    apiHandler.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintln(os.Stderr, "FAIL: Client.BaseURL path prefix is not preserved in the request URL:")
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\t"+req.URL.String())
        fmt.Fprintln(os.Stderr)
        fmt.Fprintln(os.Stderr, "\tDid you accidentally use an absolute endpoint URL rather than relative?")
        fmt.Fprintln(os.Stderr, "\tSee https://bb-github.com/google/go-bb-github/issues/752 for information.")
        http.Error(w, "Client.BaseURL path prefix is not preserved in the request URL.", http.StatusInternalServerError)
    })

    // server is a test HTTP server used to provide mock API responses.
    server := httptest.NewServer(apiHandler)

    // client is the GitHub client being tested and is
    // configured to use test server.
    client = github.NewClient(nil)
    cliUrl, _ := url.Parse(server.URL + baseURLPath + "/")
    client.BaseURL = cliUrl
    client.UploadURL = cliUrl

    return client, mux, server.URL, server.Close
}

func TestBBGithub_Invite_Pull(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false
    var permPush = lib.Dict{"permission": BBGh.permission.Pull}.Json()

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}
    user := lib.User{"Gh-bb-test"}

    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/collaborators/"+user.Str(), func(w http.ResponseWriter, r *http.Request) {
        called = true
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        tBody := strings.TrimSuffix(string(body), "\n")
        assert.True(t, tBody == permPush)
    })

    defer teardown()

    BBGh.Invite(owner, repo, user).Pull()

    assert.True(t, called, "AddCollaborator function should be called ")
}

func TestBBGithub_Invite_Push(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false
    var permPush = lib.Dict{"permission": BBGh.permission.Push}.Json()

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}
    user := lib.User{"Gh-bb-test"}

    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/collaborators/"+user.Str(), func(w http.ResponseWriter, r *http.Request) {
        called = true
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        tBody := strings.TrimSuffix(string(body), "\n")
        assert.True(t, tBody == permPush)
    })

    defer teardown()

    BBGh.Invite(owner, repo, user).Push()

    assert.True(t, called, "AddCollaborator function should be called ")
}

func TestBBGithub_Invite_Admin(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false
    var permPush = lib.Dict{"permission": BBGh.permission.Admin}.Json()

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}
    user := lib.User{"Gh-bb-test"}

    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/collaborators/"+user.Str(), func(w http.ResponseWriter, r *http.Request) {
        called = true
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        tBody := strings.TrimSuffix(string(body), "\n")
        assert.True(t, tBody == permPush)
    })

    defer teardown()

    BBGh.Invite(owner, repo, user).Admin()

    assert.True(t, called, "AddCollaborator function should be called ")
}

func TestBBGithub_Invitations(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false

    mux.HandleFunc("/user/repository_invitations", func(w http.ResponseWriter, r *http.Request) {
        called = true
        fmt.Fprintf(w, `[{"id":1}, {"id":2}]`)
    })

    defer teardown()

    got, _, _ := BBGh.Invitations()
    want := []*github.RepositoryInvitation{{ID: github.Int64(1)}, {ID: github.Int64(2)}}
    if !reflect.DeepEqual(got, want) {
        t.Errorf("Users.ListInvitations = %+v, want %+v", got, want)
    }

    assert.True(t, called, "AddCollaborator function should be called ")
}

func TestBBGithub_addDefaultBuildkitePipeline(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false
    //var permPush = Dict{"permission": BBGh.permission.Admin}.Json()
    var expectedPayload = `{"message":"Add default pipeline yaml file","content":"IyAuYnVpbGRraXRlL3BpcGVsaW5lLnltbApzdGVwczoKICAtIG5hbWU6ICI6aGFtbWVyOiBidWlsZCAiCiAgICBjb21tYW5kOiBlY2hvICJIZWxsbyB3b3JsZCIKIyAgIGFnZW50czoKIyAgICAgIHRhcmdldDogYXNzZW1ibGVyCg==","branch":"master","committer":{"name":"BuildFarm","email":"bb-buildfarm@blackbelt.hu"}}`

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}
    path := lib.Path{".buildkite/pipeline.yml"}

    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/contents/"+path.Str(), func(w http.ResponseWriter, r *http.Request) {
        called = true
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        tBody := strings.TrimSuffix(string(body), "\n")

        assert.Equal(t, expectedPayload, tBody)
        fmt.Fprint(w, `{
			"content":{
				"name":"p"
			},
			"commit":{
				"message":"m",
				"sha":"f5f369044773ff9c6383c087466d12adb6fa0828"
			}
		}`)
    })

    defer teardown()

    a, b, c := BBGh.AddDefaultBuildkitePipeline(owner, repo, path, nil)

    fmt.Println(a, b, c)
    assert.True(t, called, "addDefaultBuildkitePipeline function should be called ")
}

func TestBBGithub_AddPipelineWithJson(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    var called = false
    var expectedPayload = `{"message":"Add default pipeline yaml file","content":"IyAuYnVpbGRraXRlL3BpcGVsaW5lLnltbApzdGVwczoKICAtIG5hbWU6ICI6aGFtbWVyOiBidWlsZCAiCiAgICBjb21tYW5kOiBlY2hvICJIZWxsbyB3b3JsZCIKIyAgIGFnZW50czoKIyAgICAgIHRhcmdldDogYXNzZW1ibGVyCg==","branch":"master","committer":{"name":"BuildFarm","email":"bb-buildfarm@blackbelt.hu"}}`

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}
    path := lib.Path{".buildkite/pipeline.yml"}

    jsonStr := fmt.Sprintf(` { 
            "fileCreatingData": {
                "owner":"%v", "repository": "%v", "path": "%v"
            },
           "repoContentOpts" : %v
        }
    `, owner.Str(), repo.Str(), path.Str(),
        expectedPayload,
    )
    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/contents/"+path.Str(), func(w http.ResponseWriter, r *http.Request) {
        called = true
        defer r.Body.Close()
        body, _ := ioutil.ReadAll(r.Body)
        tBody := strings.TrimSuffix(string(body), "\n")

        assert.Equal(t, expectedPayload, tBody)
        fmt.Fprint(w, `{
			"content":{
				"name":"p"
			},
			"commit":{
				"message":"m",
				"sha":"f5f369044773ff9c6383c087466d12adb6fa0828"
			}
		}`)
    })

    defer teardown()

    a, b, c := BBGh.AddPipelineWithJson(jsonStr)

    fmt.Println(a, b, c)
    assert.True(t, called, "AddPipelineWithJson function should be called ")
}

func TestBBGithub_AcceptAllInvitations(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    want := []lib.InvError{{nil, nil, *github.Int64(1), false}, {nil, nil, *github.Int64(2), false}}

    mux.HandleFunc("/user/repository_invitations", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, `[{"id":1}, {"id":2}]`)
    })
    mux.HandleFunc("/user/repository_invitations/1", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    mux.HandleFunc("/user/repository_invitations/2", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })

    defer teardown()

    got, r, e := BBGh.Invitations()
    utErrors := BBGh.AcceptAllInvitations(got, r, e)

    for idx, ue := range utErrors {
        if !reflect.DeepEqual(ue.Id, want[idx].Id) {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", utErrors, want)
        }
        if !reflect.DeepEqual(ue.RepoInvEmpty, want[idx].RepoInvEmpty) {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", utErrors, want)
        }
    }

}

func TestBBGithub_AcceptAllInvitations_Error(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    want := []lib.InvError{{nil, nil, *github.Int64(1), false}, {nil, nil, *github.Int64(2), false}}

    mux.HandleFunc("/user/repository_invitations", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, `[{"id":1}, {"id":2}]`)
    })
    mux.HandleFunc("/user/repository_invitations/1", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusBadRequest)
    })
    mux.HandleFunc("/user/repository_invitations/2", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusBadRequest)
    })

    defer teardown()

    got, r, e := BBGh.Invitations()
    utErrors := BBGh.AcceptAllInvitations(got, r, e)

    for idx, ue := range utErrors {
        if ue.Err == nil {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", ue.Err, want[idx].Err)
        }
        if !reflect.DeepEqual(ue.RepoInvEmpty, want[idx].RepoInvEmpty) {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", ue.RepoInvEmpty, want[idx].RepoInvEmpty)
        }
    }

}

func TestBBGithub_AcceptAllInvitations_ButEmpty(t *testing.T) {
    cli, mux, _, teardown := setup()
    BBGh := NewClientWithGithubClient(cli) // 68c7976e52829d70a8a73b13ed33446c93ca50d7

    want := []lib.InvError{{nil, nil, -1, true}}
    var got []*github.RepositoryInvitation

    mux.HandleFunc("/user/repository_invitations/1", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })
    mux.HandleFunc("/user/repository_invitations/2", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(http.StatusNoContent)
    })

    defer teardown()

    _, r, e := BBGh.Invitations()

    utErrors := BBGh.AcceptAllInvitations(got, r, e)
    for idx, ue := range utErrors {
        if !reflect.DeepEqual(ue.Id, want[idx].Id) {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", utErrors, want)
        }
        if !reflect.DeepEqual(ue.RepoInvEmpty, want[idx].RepoInvEmpty) {
            t.Errorf("Users.AcceptAllInvitations = %+v, want %+v\n", utErrors, want)
        }
    }
}

func TestBBGithub_InviteWithJson(t *testing.T) {
    type fields struct {
        permission lib.Permission
        service    lib.Service
        invt       lib.Invite
        fileCreate lib.FileCreate
    }
    type args struct {
        jsonAsStr string
    }
    jsonStr := ` { "inviteData" :{
            "owner":"fefe",
            "repository": "sasa",
            "user": "keke"
    } }`
    emptyField := fields{invt: lib.Invite{lib.Owner{"f"}, lib.Repo{"s"}, lib.User{"k"}}}
    want := BBGithub{invt: lib.Invite{lib.Owner{"fefe"}, lib.Repo{"sasa"}, lib.User{"keke"}}}
    tests := []struct {
        name   string
        fields fields
        args   args
        want   BBGithub
    }{
        {"success", emptyField, args{jsonStr}, want},
        {"emptyJson", emptyField, args{"{}"}, BBGithub{}},
        {"wrongJson", emptyField, args{""}, BBGithub{}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            BBGh := BBGithub{
                permission: tt.fields.permission,
                service:    tt.fields.service,
                invt:       tt.fields.invt,
                fileCreate: tt.fields.fileCreate,
            }
            if got := BBGh.InviteWithJson(tt.args.jsonAsStr); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("fakeBBGithub.InviteWithJson() = %v, want %v", got, tt.want)
            }
        })
    }
}

func Test_setEmptyAtError(t *testing.T) {
    type args struct {
        err     error
        payload *lib.FileCreatePayload
    }
    createFCP := func() lib.FileCreatePayload {
        return lib.FileCreatePayload{
            F: lib.FileCreate{lib.Owner{"o"}, lib.Repo{"r"}, lib.Path{"p"}},
            Opts: &github.RepositoryContentFileOptions{
                Message:   github.String("Add default pipeline yaml file"),
                Content:   []byte(lib.BuildkitePipeline),
                Branch:    github.String("master"),
                Committer: &github.CommitAuthor{Name: github.String("BuildFarm"), Email: github.String("bb-buildfarm@blackbelt.hu")},
            },
        }
    }
    pay := createFCP()
    tests := []struct {
        name string
        args args
    }{
        {"atError", args{errors.New("smthng hppnd"), &pay}},
        {"NoError", args{nil, &pay}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {

            assert.Equal(t, pay, *tt.args.payload)
            setFCPToEmptyAtError(tt.args.err, tt.args.payload)

            if tt.name == "atError" {
                assert.Equal(t, lib.FileCreate{}, tt.args.payload.F)
                assert.Equal(t, &github.RepositoryContentFileOptions{}, tt.args.payload.Opts)
            } else if tt.name == "NoError" {
                assert.Equal(t, pay, *tt.args.payload)
            }

            pay = createFCP()
        })
    }
}

func Test_setOptsToDefaultWhenEmpty(t *testing.T) {
    type args struct {
        payload *lib.FileCreatePayload
    }
    pay := lib.FileCreatePayload{}
    fullPay := lib.FileCreatePayload{}
    fullPay.Opts = createDefaultRepoCntFileOpts()

    tests := []struct {
        name string
        args args
    }{
        {"notEmptyAnymore", args{&pay}},
        {"notChanged", args{&fullPay}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            fmt.Println(nil == tt.args.payload.Opts)
            if tt.name == "notEmptyAnymore" {
                assert.True(t, nil == tt.args.payload.Opts)
            } else if tt.name == "notChanged" {
                assert.True(t, nil != tt.args.payload.Opts)
            }
            setOptsToDefaultWhenEmpty(tt.args.payload)
            if tt.name == "notEmptyAnymore" {
                assert.True(t, nil != tt.args.payload.Opts)
            }
        })
    }
}

func TestMakeHttpClient(t *testing.T) {

    cliGen := func(token string) *http.Client {
        return oauth2.NewClient(
            context.Background(),
            oauth2.StaticTokenSource(&oauth2.Token{AccessToken: token}),
        )
    }
    type args struct {
        token string
    }
    tests := []struct {
        name string
        args args
        want *http.Client
    }{
        {"success", args{"sasa"}, cliGen("sasa")},
        {"noToken", args{""}, cliGen("")},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if got := MakeHttpClient(tt.args.token); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("MakeHttpClient() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestGetGithubClient(t *testing.T) {
    cliGen := func(token string) *http.Client {
        return oauth2.NewClient(
            context.Background(),
            oauth2.StaticTokenSource(&oauth2.Token{AccessToken: token}),
        )
    }
    wantNilGhCLi := github.NewClient(nil)
    wantGhCLi := github.NewClient(cliGen("sasa"))
    type args struct {
        cli *http.Client
    }
    tests := []struct {
        name string
        args args
        want *github.Client
    }{
        {"success", args{cli: cliGen("sasa")}, wantGhCLi},
        {"success", args{cli: nil}, wantNilGhCLi},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if got := GetGithubClient(tt.args.cli); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("GetGithubClient() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestBBGithub_CreateWebHook(t *testing.T) {
    cli, mux, _, teardown := setup()
    defer teardown()

    owner := lib.Owner{"BlackBeltTechnology"}
    repo := lib.Repo{"ci-integrator"}

    jsonAsStr := fmt.Sprintf(` { 
            "hookCreateData": {
                "owner":"%v", "repository": "%v"
            },
            "hook" : {
                "name": "web",
                "active": true,
                "events": [
                  "push",
                  "pull_request"
                ],
                "config": {
                  "url": "http://example.com/webhook",
                  "content_type": "json"
                }
            }
        }
    `, owner.Str(), repo.Str(),
    )

    fakeHooks := lib.HookPayload{}
    json.Unmarshal([]byte(jsonAsStr), &fakeHooks)

    fakeBBClient := NewClientWithGithubClient(cli)

    mux.HandleFunc("/repos/"+owner.Str()+"/"+repo.Str()+"/hooks", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `{"id":1}`)
    })

    type fields struct {
        permission lib.Permission
        service    lib.Service
        invt       lib.Invite
        fileCreate lib.FileCreate
    }
    type args struct {
        hookPayload lib.HookPayload
    }
    tests := []struct {
        name     string
        fields   fields
        args     args
        wantHook *github.Hook
        wantResp *github.Response
        wantErr  bool
    }{
        {"success", fields{service: fakeBBClient.service}, args{fakeHooks},
            &github.Hook{ID: github.Int64(1)},
            &github.Response{Response: &http.Response{StatusCode: 200}},
            false,
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            BBGh := BBGithub{
                permission: tt.fields.permission,
                service:    tt.fields.service,
                invt:       tt.fields.invt,
                fileCreate: tt.fields.fileCreate,
            }
            gotHook, gotResp, err := BBGh.CreateWebHook(tt.args.hookPayload)
            if (err != nil) != tt.wantErr {
                t.Errorf("BBGithub.CreateWebHook() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if !reflect.DeepEqual(gotHook, tt.wantHook) {
                t.Errorf("BBGithub.CreateWebHook() gotHook = %v, want %v", gotHook, tt.wantHook)
            }
            if !reflect.DeepEqual(gotResp.StatusCode, tt.wantResp.StatusCode) {
                t.Errorf("BBGithub.CreateWebHook() gotResp = %v, want %v", gotResp.StatusCode, tt.wantResp.StatusCode)
            }
        })
    }
}
